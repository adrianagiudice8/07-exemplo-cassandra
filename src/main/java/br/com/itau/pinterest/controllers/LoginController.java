package br.com.itau.pinterest.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.pinterest.data.PerfilEntrada;
import br.com.itau.pinterest.data.TokenSaida;
import br.com.itau.pinterest.models.Login;
import br.com.itau.pinterest.services.PerfilService;

@RestController
@RequestMapping("/login")
public class LoginController {
	@Autowired
	PerfilService perfilService;
	
	@PostMapping
	public ResponseEntity fazerLogin(@RequestBody Login login) {
		Optional<String> tokenOptional = perfilService.fazerLogin(login);
		
		if(tokenOptional.isPresent()) {
			TokenSaida tokenSaida = new TokenSaida();
			tokenSaida.setToken(tokenOptional.get());
			
			return ResponseEntity.ok(tokenSaida);
		}
		
		return ResponseEntity.status(400).build();
	}
}
