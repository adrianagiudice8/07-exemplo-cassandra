package br.com.itau.pinterest.controllers;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import br.com.itau.pinterest.models.Login;
import br.com.itau.pinterest.services.PerfilService;

@RunWith(SpringRunner.class)
@WebMvcTest(LoginController.class)
public class LoginControllerTeste {

	@MockBean
	PerfilService perfilService;
	
	@Autowired
	MockMvc mockMvc;
	
	@Test
	public void testarLogin() throws Exception {
		String token = "tokenlegal";
		
		when(perfilService.fazerLogin(any(Login.class))).thenReturn(Optional.of(token));
		
		MockHttpServletRequestBuilder request = post("/login")
			.contentType(MediaType.APPLICATION_JSON_UTF8)
			.content("{\"email\": \"teste@teste.com\", \"senha\": \"teste123\"}");
		
		mockMvc.perform(request)
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.token", equalTo(token)));
	}
	
	@Test
	public void testarLoginInvalido() throws Exception {	
		when(perfilService.fazerLogin(any(Login.class))).thenReturn(Optional.empty());
		
		MockHttpServletRequestBuilder request = post("/login")
			.contentType(MediaType.APPLICATION_JSON_UTF8)
			.content("{\"email\": \"teste@teste.com\", \"senha\": \"teste123\"}");
		
		mockMvc.perform(request)
			.andExpect(status().is4xxClientError());
	}
}
